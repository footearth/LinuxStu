# Aoe ([ATA over Ethernet](http://zh.wikipedia.org/wiki/ATA_over_Ethernet))

## [Server - vblade](http://sourceforge.net/projects/aoetools/files/vblade/)

    axel http://jaist.dl.sourceforge.net/project/aoetools/vblade/vblade-21.tgz
    mkdir -p ./build/usr/sbin
    mkdir -p ./build/usr/share/man/man8

    make
    make install

### [Driver](http://support.coraid.com/support/linux/)

    axel http://support.coraid.com/support/linux/aoe6-84.tar.gz
    make ; make install

    grep ATA_OVER /boot/config-`uname -r`
    lsmod | grep aoe

    modprobe aoe
    modprobe aoe aoe_iflist=eth0

### aoetools

    axel http://jaist.dl.sourceforge.net/project/aoetools/aoetools/aoetools-36.tar.gz

    yum install aoetools
    dd if=/dev/zero of=disk5.img bs=1024k count=300
    ~/tmp/vblade/vblade-21/build/sbin/vbladed 0 1 eth1 disk5.img

    aoe-version
    aoe-discover
    aoe-stat

### vblade

    axel http://jaist.dl.sourceforge.net/project/aoetools/vblade/vblade-21.tgz
    make ; make install

    killall vblade

    ./vbladed 0 0 eth1 PE.iso
    ./vbladed 0 1 eth1 Win7.iso
    ./vbladed 0 2 eth1 disk.img

## Client

    # TODO
    vboxmanage storageattach Redhat --storagectl "SATA" --port 0 --type hdd --medium iscsi --server 192.168.199.109 --target "iqn.tgt1" --lun 1 --tport 3260

### [WinAoe](http://winaoe.org/)

    bcdedit -set TESTSIGNING ON
    bcdedit -set loadoptions DDISABLE_INTEGRITY_CHECKS

    aoe.exe scan
    aoe.exe mount

## 参考资料

* [Linux 下配置 ATA-over-Ethernet（AoE） 存储系统](http://www.ibm.com/developerworks/cn/linux/l-cn-aoe/)
* [ATA-over-Ethernet（AoE）介紹：一種 iSCSI 與 Fibre Channel 之替代方案，TeraByte 等級磁碟儲存系統](http://www.babyface2.com/NetAdmin/07200608AoE/)
