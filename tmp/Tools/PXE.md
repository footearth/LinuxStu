# PXE

## [iPXE](http://ipxe.org/)

    yum install genisoimage

    git clone git://git.ipxe.org/ipxe.git
    cd ipxe/src ; make
    cd ipxe/src ; make bin/ipxe.iso
    cd ipxe/src ; make bin/ipxe.usb ; dd if=bin/ipxe.usb of=/dev/sdX
    cd ipxe/src ; make bin/undionly.kpxe

    axel http://boot.ipxe.org/ipxe.iso
    axel http://boot.ipxe.org/undionly.kpxe

### [command](http://ipxe.org/cmd)

    # Download an image
    initrd or module or imgfetch

    # Download and select an executable image
    kernel or imgload or imgselect

    # Download and boot an executable image
    boot or chain or imgexec

    imgstat

### sanboot

    # Boot from local hard disk
    sanboot --no-describe --drive 0x80
    ## cd-rom device number is 0xA0, but it doesn't boot

    # Boot from an iSCSI target
    sanboot iscsi:10.0.4.1:::1:iqn.2010-04.org.ipxe.dolphin:storage

    # Boot from an HTTP target
    sanboot http://boot.ipxe.org/freedos/fdfullcd.iso

    # sanboot from PE iso
    dhcp or dhcp netN
    set keep-san 1
    sanhook --drive 0x81 aoe:eN.N or iscsi:ip:::lunN:iqn.tgtN
    sanboot --no-describe --keep aoe:eN.N or iscsi:ip:::lunN:iqn.tgtN

### WINDOWS 安装

* [必须要使用 windows 部署工具](http://ipxe.org/howto/wds_iscsi)

## [gPXE](http://etherboot.org/) -- 项目 止于 2011/08/16

* [Download](http://rom-o-matic.net/gpxe/gpxe-git/gpxe.git/contrib/rom-o-matic/)

# 无盘

* [isharedisk](http://www.isharedisk.com/index_cn.html)
* [ccboot](http://www.ccboot.com/diskless-boot.htm)
