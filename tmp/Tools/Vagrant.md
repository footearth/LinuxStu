# Vagrant

## [下载](http://downloads.vagrantup.com/)

* [Vagrant 镜像](http://www.vagrantbox.es/)

## 安装镜像

    ## vagrant box add ${vmName} ${repobox}
    $ vagrant box add base http://developer.nrel.gov/downloads/vagrant-boxes/CentOS-6.4-x86_64-v20130731.box

    ## 初始化
    $ vagrant init

    ## 这个是真正的启动
    $ vagrant up

    ## 登录虚拟机
    $ vagrant ssh

## 常用命令

    # 添加一个 box
    $ vagrant box add NAME URL

    # 查看本地已添加的 box
    $ vagrant box list

    # 删除本地已添加的 box，如若是版本1.0.x，执行$ vagrant box remove  NAME
    $ vagrant box remove NAME virtualbox

    # 初始化，实质应是创建 Vagrantfile 文件
    $ vagrant init NAME

    # 启动虚拟机
    $ vagrant up

    # 关闭虚拟机
    $ vagrant halt

    # 销毁虚拟机
    $ vagrant destroy

    # 重启虚拟机
    $ vagrant reload

    # 当前正在运行的 VirtualBox 虚拟环境打包成一个可重复使用的 box
    $ vagrant package

    # 进入虚拟环境
    $ vagrant ssh
