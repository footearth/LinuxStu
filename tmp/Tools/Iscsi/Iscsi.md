# ISCSI

    yum install scsi-target-utils

## Server

    mkdir –p /opt/disks

    cd /opt/disks

    # DD 命令创建虚拟磁盘
    # 100   100M
    # 1000  1G
    # 10000 10G

    # dd
    dd if=/dev/zero of=diskN bs=1024k count=SIZE

    # losetup
    losetup /dev/loopN diskN
    losetup -d /dev/loopN
    losetup -a

    # pv
    pvcreate /dev/loopN
    pvscan
    pvdisplay

    # vg
    vgcreate vg_base /dev/loop1 /dev/loop2 /dev/loop3 /dev/loop4
    vgchange -a y vg_base
    vgscan
    vgdisplay

    # lv
    lvcreate -L 39984 -n lv_base vg_base
    lvscan
    lvdisplay
    fdisk -l

    # tgtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 1 -b /dev/vg_base/lv_base

    # tgtd
    service tgtd start

    tgtadm --lld iscsi --op new --mode target --tid 1 -T iqn.tgt1
    tgtadm --lld iscsi --op bind --mode target --tid 1 -I 192.168.199.146

    tgtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 1 -b /vagrant/tftp/WIN8PE.iso
    tgtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 2 -b /vagrant/tftp/x86-WIN7-6in1.iso
    tgtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 3 -b /root/disk/disk42G.img

    tgtadm --lld iscsi --op bind --mode target --tid 1 -I ALL

    tgtadm --lld iscsi --mode target --op show | less

## Client

    # vbox
    vboxmanage storageattach Redhat --storagectl "SATA" --port 0 --type hdd --medium iscsi --server 192.168.199.109 --target "iqn.tgt1" --lun 1 --tport 3260

    # linux iscsi cli
    iscsiadm -m discovery -t sendtargets -p 192.168.199.146:3260
        192.168.199.146:3260,1 iqn.tgt1
    iscsiadm -m node –T iqn.tgt1 -p 192.168.199.146:3260 -l
    iscsiadm -m node –T iqn.tgt1 -p 192.168.199.146:3260 -u

    # windows iscsi cli
    iscsicli QAddTargetPortal <Portal IP Address>
    iscsicli ListTargets
    iscsicli QloginTarget <target_iqn>
    iscsicli ReportTargetMappings
    iscsicli PersistentLoginTarget iqn.tgt1 T * * * * * * * * * * * * * * * 0
    iscsicli ListPersistentTargets
