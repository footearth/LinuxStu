# Dnsmasq

## install

    yum install dnsmasq syslinux-tftpboot

## /etc/dnsmasq.conf

    interface=eth1

    # DHCP
    expand-hosts
    domain=footearth.org
    dhcp-range=192.168.199.100,192.168.199.254,255.255.255.0,12h
    dhcp-option=option:router,192.168.199.146
    dhcp-authoritative
    log-dhcp

    # TFTP
    enable-tftp
    tftp-root=/tftpboot/

    # DNS
    log-queries

    # PXE

    # dhcp-boot=pxelinux.0,boothost,192.168.199.146
    dhcp-boot=undionly.0,boothost,192.168.199.146

    #dhcp-match=gpxe,175
    #dhcp-option=175,8:1:1
    #dhcp-boot=net:#ipxe,undionly.0
    #dhcp-boot=tag:!ipxe,undionly.0
    #dhcp-option=net:gpxe,17,"aoe:e0.1"
    #dhcp-boot=pxelinux.0

    dhcp-no-override

### back conf

    cat /etc/dnsmasq.conf | grep -v '^#' | grep -v '^$' > dnsmasq.conf

## syslinux

    /tftpboot/pxelinux.cfg/default

        UI vesamenu.c32

        LABEL WINPE.iso
            LINUX memdisk
            INITRD /WINPE.iso
            APPEND iso raw

### cmd

    /usr/sbin/dnsmasq
    service dnsmasq restart
