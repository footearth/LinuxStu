# Cobbler

## 安装

    yum install cobbler cobbler-web
    yum install dhcp bind xinetd debmirror pykickstart cman tftp
    service iptables stop
    chkconfig iptables off
    openssl passwd -1 -salt 'random-phrase-here' 'netserver'
    cobbler get-loaders

## 检查

    cobbler check

## 重启服务

    #!/bin/bash
    setenforce 0
    cobbler sync
    service cobblerd restart
    service httpd restart
    service dhcpd restart
    service named restart
    service xinetd restart

## 添加镜像

    mount -o loop /vagrant/CentOS-6.4-x86_64-bin-DVD1.iso /mnt/
    cobbler import --path=/mnt --name=CentOS-6.4-x86_64 --arch=x86_64

## tftpd time out

    $ vi /etc/cobbler/tftpd.template

        server_args = $args
