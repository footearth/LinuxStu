# CentOS

## 网络

### 有线

### 无线

## 安装源

### 本地光盘源

#### 无网络

yum --disablerepo=\* --enablerepo=c6-media [command]

#### 有网络

yum --enablerepo=c6-media [command]

    $ mkdir -p /media/cdrom
    $ mount /dev/cdrom /media/cdrom
    $ vi ~/.bashrc
        alias yum='yum --disablerepo=\* --enablerepo=c6-media'

### 添加外部源

    ## RPMFORGE
    $ wget http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.i686.rpm
    $ rpm -ivh rpmforge-release-0.5.3-1.el6.rf.i686.rpm

    ## EPEL
    $ wget http://mirror.bjtu.edu.cn/fedora-epel/6/i386/epel-release-6-8.noarch.rpm
    $ rpm -ivh epel-release-6-8.noarch.rpm

    $ rpm -Uvh http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.i686.rpm && rpm -Uvh http://mirror.bjtu.edu.cn/fedora-epel/6/i386/epel-release-6-8.noarch.rpm

## 关闭防火墙

    $ chkconfig iptables off
    $ service iptables stop

## 安装常用软件包

    $ yum install mtr tree htop hdparm ifstat iftop patch
    $ yum install axel lftp tmux fish byobu vim man
    $ yum install git

## 配置 fish 环境变量

    $ touch ~/.config/fish/config.fish
    $ vim ~/.config/fish/config.fish

    # env
    set -x PATH $PATH

    set -x LESS_TERMCAP_mb \e'[01;31m'          # begin blinking
    set -x LESS_TERMCAP_md \e'[01;38;5;74m'     # begin bold
    set -x LESS_TERMCAP_me \e'[0m'              # end mode
    set -x LESS_TERMCAP_se \e'[0m'              # end standout-mode
    set -x LESS_TERMCAP_so \e'[38;5;246m'       # begin standout-mode - info box
    set -x LESS_TERMCAP_ue \e'[0m'              # end underline
    set -x LESS_TERMCAP_us \e'[04;38;5;146m'    # begin underline

    # alias
    function vi
        /usr/bin/vim $argv
    end

## RPM

    # 安装包
    rpm -ivh packageName
    # 检查文件属于哪个包
    rpm -qif fileName
    # 检查安装包文件列表
    rpm -ql packageName
