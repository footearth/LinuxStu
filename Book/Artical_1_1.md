# 在 Virtual Box 中 安装 ArchLinux 到 USB 虚拟磁盘

## 新建虚拟机

### USB 设备挂载为虚拟磁盘

    # 查看所有磁盘
    diskutil list
    # 显示指定磁盘信息
    diskutil info /dev/disk1 | grep uuid
    # 显示指定分区信息
    diskutil info /dev/disk1s1 | grep uuid
    # 挂载和卸载设备
    diskutil mountDisk /dev/disk1
    diskutil unmountDisk /dev/disk1
    # 指定设备属主
    sudo chown FooTearth /dev/disk1
    # 创建 USB 设备为虚拟磁盘
    VBoxManage internalcommands createrawvmdk -filename ./usbDisk1.vmdk -rawdisk /dev/disk1
    
### 网卡配置

通常情况我们会配两个网卡

一个配成 桥接网卡 和 主机网卡 桥接

一个配成 Host-Only 模式
    
## 光盘安装
    
### 分区

    # 查看系统设备情况
    lsblk

#### 使用 UEFI + PGT

* parted


    # 列出 分区 列表
    print
    # 删除 分区
    rm {分区号}

* cdisk


    # 列出 分区 列表
    p
    # 删除 分区
    d {分区号}
    # 新建 分区
    n
    # 应用 更改
    w

* cgdisk

#### 分区策略

第一分区 500M 挂载 /boot 分区  EF00
第二分区 4G   挂载 swap 分区   8200
第三分区 剩余空间 挂载 / 分区   8300

### 磁盘格式化

    mkfs.vfat -F32 /dev/sda1
    mkswap /dev/sda2
    mkfs.ext4 /dev/sda3

### 挂载分区

    mount /dev/sda3 /mnt
    mkdir -p /mnt/boot
    mount /dev/sda1 /mnt/boot
    swapon /dev/sda2

### 有线网络

    # 显示 网卡
    ip link list
    # 设置 启动 关闭 网卡
    ip link set {dev} down
    ip link set {dev} up

### 无线网络

    pacman -S iw wpa_supplicant wifi-menu dialog

### 修改源

    # 将 163 源 放到 最前面
    vi /etc/pacman.d/mirrorlist
    
### 更新软件包列表

    pacman -Syy

### 安装基本系统

    pacstrap /mnt base

### 生成 fstab

    genfstab -U -p /mnt >> /mnt/etc/fstab

### chroot

    arch-chroot /mnt /bin/bash
    
### 安装软件包

    pacman -S base-devel vim tmux fish zsh
    
### 配置基本系统

    # 配置 locale
    # 取消 en_US zh_CN 组前的注释
    vi /etc/locale.gen
    locale-gen 
      
    vi /etc/locale.conf
    LANG=en_US.UTF-8
    
    ln -s /usr/share/soneinfo/Asia/Shanghai /etc/localtime
    hwclock --systohc -utc
    
    vi /etc/hostname
    FT-ARCH-US
    
    passwd
    
### 安装启动器 Grub

    pacman -S grub efibootmgr
    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch_grub --recheck --debug
    grub-mkconfig -o /boot/grub/grub.cfg
    
## 系统配置

### UEFI shell

    # 列出所有设备
    map
    # 进入指定设备
    fs0:
    # 进入 boot 文件夹
    cd EFI/arch_grub
    # 启动 grub
    grubx64.efi
    
### 开启网络服务

    # 启动 dhcp 服务
    systemctl start dhcpcd.service
    # 自动启动 dhcp 服务
    systemctl enable dhcpcd.service
    
### 添加用户

    useradd -m -g users -G wheel -s /usr/bin/fish footearth
    passwd footearth
    visudo
    # 去掉 wheel sudo 权限的 注释
    
### 安装 开发 工具

    pacman -S git perl python ruby nodejs
    

### 配置 bash fish vim zsh

#### fish

    set PATH $PATH /usr/bin/core_perl

#### zsh

    # 安装 oh-my-zsh
    curl -L http://install.ohmyz.sh | sh
    
### 安装 包 管理器

    # 创建目录
    cd
    mkdir GIT bin
    set PATH $PATH ~/bin
    # 安装 cower
    pacman -S yajl
    git clone https://github.com/falconindy/cower
    make
    ln -s ~/GIT/cower/cower
    # 安装 owl
    git clone https://github.com/baskerville/owlman
    ln -s ~/GIT/owlman/owlman ~/bin/owl
    # 安装 burp
    owl install burp
    
## 配置桌面

### 安装显示驱动

    pamcan -S mesa virtualbox-guest-utils
    
    modprobe -a vboxguest vboxsf vboxvideo
    systemctl start vboxservice.service
    systemctl enable vboxservice.service
    
    vi /etc/modules-load.d/virtualbox.conf
    vboxguest
    vboxsf
    vboxvideo
    
### 安装桌面

    pacman -S xorg-server xorg-server-utils xorg-xinit
    pacman -S xf86-input-synaptics
    pacman -S ttf-dejavu wqy-microhei
    
    # bspwm sxhkd
    owl install bspwm-git sxhkd-git
    git clone https://github.com/baskerville/bspwm
    git clone https://github.com/baskerville/sxhkd
    mkdir -p ~/.config/{bspwm,sxhkd}
    cp ~/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
    cp ~/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc
    export PANEL_FIFO="/tmp/panel-fifo"
    
### 安装桌面终端

    owl install terminator tilda
    
### 安装常用工具

    pacman -S axel lftp htop
    
    ## randrctl dwb sxiv phototonic
